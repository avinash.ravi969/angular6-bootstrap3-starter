import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './layout.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    LayoutRoutingModule
  ],
  declarations: [
    HomeComponent,
    LayoutComponent
  ]
})
export class LayoutModule { }
